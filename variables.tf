variable creds {
  default     = ".json"
  description = "description"
}

variable proj_id {
  type        = string
  default     = "focal-freedom-292312"
  description = "description"
}

variable master_name {
  type        = string
  default     = "jenkins-master"
  description = "description"
}

variable slave_name {
  type        = string
  default     = "jenkins-slave"
  description = "description"
}

variable region {
  type        = string
  default     = "us-east1"
  description = "description"
}

variable zone {
  type        = string
  default     = "us-east1-b"
  description = "description"
}

variable fw_name {
  type        = string
  default     = "jenkins"
  description = "description"
}

variable ssh_pub {
  type        = string
  default     = "C:/Users/dimal/.ssh/id_rsa.pub"
  description = "description"
}

variable ssh_prv {
  type        = string
  default     = "~/.ssh/id_rsa"
  description = "description"
}


variable ssh_user {
  type        = string
  default     = "dimalenchuk"
  description = "description"
}

variable network_name {
  type        = string
  default     = "jenkins-network"
  description = "description"
}

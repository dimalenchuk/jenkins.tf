provider "google" {
  credentials = file(var.creds)
  project = var.proj_id
  region  = var.region
  zone    = var.zone
  user_project_override = true
}

resource "google_compute_firewall" "default" {
  name    = var.fw_name
  network = google_compute_network.vpc_network.name
  allow {
      protocol = "tcp"
      ports = ["22","443","8080"]
  }
}

resource "google_compute_instance" "jenkins_master" {
  name = var.master_name
  machine_type = "e2-medium"
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  tags = ["${google_compute_firewall.default.name}"]

  network_interface {
    network = google_compute_network.vpc_network.self_link
    access_config {
    }
  }
  metadata = {
    ssh-keys = "${var.ssh_user}:${file(var.ssh_pub)}"
  }
  provisioner "file" {
    source      = "install-jenkins.sh"
    destination = "~/install-jenkins.sh"
  }
  provisioner "remote-exec" {
      inline = [
          "chmod +x ~/install-jenkins.sh",
          "sudo ~/install-jenkins.sh"
      ]
  }
  connection {
          type = "ssh"
          user = var.ssh_user
          private_key = file(var.ssh_prv)
          host = self.network_interface.0.access_config.0.nat_ip
  }
}

resource "google_compute_instance" "jenkins_slave" {
  name = "${var.slave_name}-${count.index+1}"
  machine_type = "e2-medium"
  count = 2
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  tags = ["${google_compute_firewall.default.name}"]

  network_interface {
    network = google_compute_network.vpc_network.self_link
    access_config {
    }
  }
  metadata = {
    ssh-keys = "${var.ssh_user}:${file(var.ssh_pub)}"
  }
  provisioner "file" {
    source      = "install-java.sh"
    destination = "~/install-java.sh"
  }
  provisioner "remote-exec" {
      inline = [
          "chmod +x ~/install-java.sh",
          "sudo ~/install-java.sh"
      ]
  }
  connection {
          type = "ssh"
          user = var.ssh_user
          private_key = file(var.ssh_prv)
          host = self.network_interface.0.access_config.0.nat_ip
  }
}
output "ip_master" {
    value = "${google_compute_instance.jenkins_master.network_interface.0.access_config.0.nat_ip}:8080"
}

output "ip_slaves" {
  value = "${google_compute_instance.jenkins_slave.*.network_interface.0.access_config.0.nat_ip}"
}



resource "google_compute_network" "vpc_network" {
  name                    = var.network_name
  auto_create_subnetworks = "true"
}